#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import ssl
import sleekxmpp

import config

from argparse import ArgumentParser

from fbchat import Client
from fbchat.models import *


class MUCBot(sleekxmpp.ClientXMPP):
    def __init__(self, jid, password):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)

        self.default_room_nick = config.xmpp_jid.split('@')[0]
        self.previous_nick = self.default_room_nick
        self.nick = self.default_room_nick

        self.add_event_handler("session_start", self.start)
        self.add_event_handler("groupchat_message", self.muc_message)

    def set_muc_nick(self, muc, nick):
        self.previous_nick = self.nick
        self.nick = nick
        xmpp.send_presence(pto='{}/{}'.format(muc, nick))

    def set_default_muc_nick(self, muc):
        self.previous_nick = self.nick
        self.nick = self.default_room_nick
        xmpp.send_presence(pto='{}/{}'.format(muc, self.default_room_nick))

    def start(self, event):
        self.get_roster()
        self.send_presence()
        for room in xmpp_muc_to_facebook:
            self.plugin['xep_0045'].joinMUC(room, self.nick, wait=True)

    def muc_message(self, msg):
        if msg['mucnick'] != self.nick and msg['mucnick'] != self.previous_nick and msg['body']:
            message_to_facebook = "[XMPP] {}: {}".format(msg['mucnick'], msg['body'])
            facebook_client.send(Message(text=message_to_facebook), thread_id=xmpp_muc_to_facebook[msg['from'].bare],
                                 thread_type=ThreadType.GROUP)


class FacebookBot(Client):
    def onMessage(self, author_id, message_object, thread_id, thread_type, **kwargs):
        self.markAsDelivered(author_id, thread_id)

        logging.info("Received a new message:")
        if message_object.text:
            logging.info("Message content is: " + message_object.text)
        logging.info("Author is: " + author_id)
        our_sender_user = self.fetchUserInfo(author_id)[author_id]
        logging.info("He really is: " + our_sender_user.name)
        logging.info("thread_id: " + thread_id)
        logging.info("thread_type.name: " + thread_type.name)

        message_should_be_handled = thread_id in config.facebook_to_xmpp_muc# and author_id != self.uid

        if message_should_be_handled:
            logging.info("Sending message to XMPP MUC")

            destination_muc = config.facebook_to_xmpp_muc[thread_id]

            xmpp.set_muc_nick(destination_muc, our_sender_user.name)

            if message_object.attachments:
                logging.info("Message contains attachments")

                for attachment in message_object.attachments:
                    if type(attachment) is ImageAttachment:
                        file_url = attachment.large_preview_url
                    elif type(attachment) is VideoAttachment:
                        file_url = attachment.preview_url

                    message = xmpp.Message(sto=destination_muc, stype='groupchat')

                    message['body'] = file_url
                    message['oob']['url'] = file_url

                    message.send()

            if message_object.text:
                xmpp.send_message(mto=destination_muc, mbody=message_object.text, mtype='groupchat')

            xmpp.set_default_muc_nick(destination_muc)


if __name__ == '__main__':
    parser = ArgumentParser()

    parser.add_argument("-q", "--quiet", help="set logging to ERROR",
                        action="store_const", dest="loglevel",
                        const=logging.ERROR, default=logging.INFO)
    parser.add_argument("-d", "--debug", help="set logging to DEBUG",
                        action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.INFO)

    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel,
                        format='%(levelname)-8s %(message)s')

    xmpp_muc_to_facebook = {v: k for k, v in config.facebook_to_xmpp_muc.items()}

    xmpp = MUCBot(config.xmpp_jid, config.xmpp_password)
    xmpp.ssl_version = ssl.PROTOCOL_TLSv1_2
    xmpp.register_plugin('xep_0030')  # Service Discovery
    xmpp.register_plugin('xep_0045')  # Multi-User Chat
    xmpp.register_plugin('xep_0199')  # XMPP Ping
    xmpp.register_plugin('xep_0066')  # Out-of-band Data

    xmpp.connect()
    xmpp.process()

    facebook_client = FacebookBot(config.facebook_email, config.facebook_password)
    facebook_client.listen()
